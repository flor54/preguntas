from django.conf.urls import url
from django.urls import path, re_path
from .views import *


app_name = 'preguntas-api'

urlpatterns = [
    path('', MixPregunta, name='list'),
	path('pregunta', crearPregunta, name='pregunta'),
]   