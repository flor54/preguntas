from django import template
from preguntas.models import Respuesta
from django.contrib.auth.models import User
 

register = template.Library()
class ObjRespuesta:
	contenido = ''
	user = ''
	def __init__(self,cont,user):
		self.contenido = cont
		self.user = user
		


@register.filter(name='comentarios')
def comentarios(pk):
	qs = Respuesta.objects.filter(id=pk).values('contenido','user')
	lista =[]
	i = 0
	for obj in qs:

		info = ObjRespuesta(qs[i].get('contenido'),qs[i].get('user'))
		lista.append(info)
		i+=1
	return lista

register.filter('comentarios', comentarios)