from django.shortcuts import render
from .models import *
from .forms import *
from django.shortcuts import HttpResponseRedirect
from django.contrib.auth.models import User

from django.contrib import messages

# User = settings.AUTH_USER_MODEL

def crearPregunta(request):
    form = PreguntaForm(request.POST or None)
    print(request.POST)
    if(not(form.is_valid)):
        return HttpResponseRedirect('')
    context ={
        'form':form
    }

    return render(request, 'preguntaas-form.html',context)

def MixPregunta(request):
	form_create = PreguntaForm(request.POST or None)


	search_id = request.POST.get('id_pregunta_input', None)


	# print(request.POST.get('id_pregunta_input', None))



	form = RespuestaForm(request.POST or None)
	if(form.is_valid()):
		ins = form.save(commit=False)
		ins.pregunta_id = search_id
		ins = form.save()

		
	form_create = PreguntaForm(request.POST or None)
	if(request.method == 'POST'):
		if(form_create.is_valid()):
			ins = form_create.save(commit=False)
			ins = form_create.save()
			messages.success(request, 'Pregunta creada')
		else:
			return HttpResponseRedirect('/')


	instance = Pregunta.objects.all()


	#insert en PregutnaRespuesta:
	# ins1 = instance.get(id=1)
	# ins2 = Respuesta.objects.get(id=1)
	# obj = PregutnaRespuesta()
	# obj.pregunta = ins1
	# obj.respuestas = ins2
	# obj.save()
	# -- se va a guardar y no va a haber problema d PK xq la PK es ID y no una FK

	context = {
	'obj_list' : instance,
	'form':form,
	'form_create':form_create,
	}
	return render(request,'index.html',context)