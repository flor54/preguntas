from django.db import models
from django.db.models.signals import pre_save
from .utils import unique_slug_generator
from django.contrib.auth.models import User
# Create your models here.

class Pregunta(models.Model):
	title   = models.CharField(max_length=120)
	content = models.TextField()
	user  = models.ForeignKey(User, default=1,on_delete=models.CASCADE,null=True,blank=True)
	slug = models.SlugField(blank=True, null=True)


	def __str__(self):
		return self.title
		
		
	@property
	def get_user(self):
		return User.objects.get(id=self.user)
		
	@property
	def get_answers(self):
		return Respuesta.objects.filter(pregunta_id=self.id)
		

class Respuesta(models.Model):
	pregunta  = models.ForeignKey(Pregunta,on_delete=models.CASCADE,null=True)
	contenido = models.TextField(blank=True)
	user  = models.ForeignKey(User, default=1,on_delete=models.CASCADE,null=True,blank=True)
	


	def __str__(self):
		return (str( self.id))
		
	def get_user(self):
		return User.objects.get(id=self.user)

	
	
	
def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(pre_save_post_receiver, sender=Pregunta)