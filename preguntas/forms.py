from django import forms

from .models import Pregunta,Respuesta
from django.contrib.auth.models import User


# users = User.objects.all().values('username')

# list =[]
# i = 0
# for obj in users:
	# list.append((str(i),obj['username']))
	
	# i+=1
	
class RespuestaForm(forms.ModelForm):
	# choice_field = forms.ChoiceField(widget=forms.RadioSelect, choices=list)
	class Meta:
		model = Respuesta
		fields = [
			'contenido',
			'user',
		]
		
	def clean_user(self):
		data = self.cleaned_data['user']
		# do some stuff
		if(data is None):
			data = None
		return data
		
	
class PreguntaForm(forms.ModelForm):

	class Meta:
		model = Pregunta
		fields=[
			'title',
            'content',
			'user'
		]
	
	
	
	
	
	# class RenewBookForm(forms.Form):
#     renewal_date = forms.CharField(help_text="")